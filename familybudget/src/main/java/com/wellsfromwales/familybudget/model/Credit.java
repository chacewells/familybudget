package com.wellsfromwales.familybudget.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CREDIT")
public class Credit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CREDIT_ID")
	private Long id;

	@Column(name="CREDIT_TRANSACTION_DATE")
	private Long transactionDate;
	
	@Column(name="CREDIT_DESCRIPTION")
	private String description;
	
	@Column(name="CREDIT_AMOUNT_DOLLARS")
	private BigDecimal amount;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="CREDIT_PARTY_ID")
	private Party party;
	
//	TODO: set up additional *-to-* attributes for all entities
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="CREDIT_CATEGORY_ID")
	private CreditCategory creditCategory;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return new Date(transactionDate);
	}

	public void setDate(Date date) {
		this.transactionDate = date.getTime();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public CreditCategory getCreditCategory() {
		return creditCategory;
	}

	public void setCreditCategory(CreditCategory creditCategory) {
		this.creditCategory = creditCategory;
	}

}
