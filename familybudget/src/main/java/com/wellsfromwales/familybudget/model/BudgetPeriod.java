package com.wellsfromwales.familybudget.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BUDGET_PERIOD")
public class BudgetPeriod {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="BUDGET_PERIOD_ID")
	private Long id;
	
	@Column(name="BUDGET_PERIOD_START_DATE")
	private Long startDate;
	
	@Column(name="BUDGET_PERIOD_END_DATE")
	private Long endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

}
