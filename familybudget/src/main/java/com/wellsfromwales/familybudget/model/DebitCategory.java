package com.wellsfromwales.familybudget.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="DEBIT_CATEGORY")
public class DebitCategory {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="DEBIT_CATEGORY_ID")
	private Long id;
	
	@Column(name="DEBIT_CATEGORY_NAME")
	private String name;
	
	@OneToMany(mappedBy="category")
	private Collection<Debit> debits;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Debit> getDebits() {
		return debits;
	}

	public void setDebits(Collection<Debit> debits) {
		this.debits = debits;
	}
	
}
