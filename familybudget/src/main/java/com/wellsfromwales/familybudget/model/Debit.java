package com.wellsfromwales.familybudget.model;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="DEBIT")
public class Debit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="DEBIT_ID")
	private Long id;

	@Column(name="DEBIT_TRANSACTION_DATE")
	private Long transactionDate;
	
	@Column(name="DEBIT_DESCRIPTION")
	private String description;
	
	@Column(name="DEBIT_AMOUNT_DOLLARS")
	private BigDecimal amount;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="DEBIT_PARTY_ID")
	private Party party;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="DEBIT_CATEGORY_ID")
	private DebitCategory category;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTransactionDate() {
		return new Date(transactionDate);
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate.getTime();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public DebitCategory getCategory() {
		return category;
	}

	public void setCategory(DebitCategory category) {
		this.category = category;
	}
	
}
