package com.wellsfromwales.familybudget.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PLANNED_EXPENSE")
public class PlannedExpense {
	
	@Id
	@GeneratedValue
	@Column(name="PLANNED_EXPENSE_ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="DEBIT_CATEGORY_ID")
	private DebitCategory debitCategory;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="BUDGET_PERIOD_ID")
	private BudgetPeriod budgetPeriod;
	
	@Column(name="PLANNED_EXPENSE_AMOUNT_DOLLARS_AND_CENTS")
	private BigDecimal amount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DebitCategory getDebitCategory() {
		return debitCategory;
	}

	public void setDebitCategory(DebitCategory debitCategory) {
		this.debitCategory = debitCategory;
	}

	public BudgetPeriod getBudgetPeriod() {
		return budgetPeriod;
	}

	public void setBudgetPeriod(BudgetPeriod budgetPeriod) {
		this.budgetPeriod = budgetPeriod;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
