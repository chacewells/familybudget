package com.wellsfromwales.familybudget.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PLANNED_INCOME")
public class PlannedIncome {
	
	@Id
	@GeneratedValue
	@Column(name="PLANNED_INCOME_ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CREDIT_CATEGORY_ID")
	private CreditCategory creditCategory;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="BUDGET_PERIOD_ID")
	private BudgetPeriod budgetPeriod;
	
	@Column(name="PLANNED_INCOME_AMOUNT_DOLLARS_AND_CENTS")
	private BigDecimal amount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CreditCategory getCreditCategory() {
		return creditCategory;
	}

	public void setCreditCategory(CreditCategory creditCategory) {
		this.creditCategory = creditCategory;
	}

	public BudgetPeriod getBudgetPeriod() {
		return budgetPeriod;
	}

	public void setBudgetPeriod(BudgetPeriod budgetPeriod) {
		this.budgetPeriod = budgetPeriod;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
