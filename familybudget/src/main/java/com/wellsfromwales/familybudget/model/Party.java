package com.wellsfromwales.familybudget.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PARTY")
public class Party {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="PARTY_ID")
	private Long id;
	
	@Column(name="PARTY_NAME")
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
