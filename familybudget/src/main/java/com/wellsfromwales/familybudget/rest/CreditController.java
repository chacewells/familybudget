package com.wellsfromwales.familybudget.rest;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wellsfromwales.familybudget.model.Credit;
import com.wellsfromwales.familybudget.model.CreditCategory;
import com.wellsfromwales.familybudget.model.Party;
import com.wellsfromwales.familybudget.service.CreditService;

@RestController
@RequestMapping("/rest/credit")
public class CreditController {
	
	@Autowired
	private CreditService creditService;
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}", produces="application/json")
	public Credit getMappingById(@PathVariable("id") Long id) {
		Credit credit = new Credit();
		
		credit.setAmount(new BigDecimal("55.00"));
		credit.setCreditCategory( ((Supplier<CreditCategory>)() -> {
			CreditCategory cCat = new CreditCategory();
			cCat.setId(1L);
			cCat.setName("groceries");
			return cCat;
		}).get() );
		credit.setDate(new Date(System.currentTimeMillis()));
		credit.setDescription("Lots of cheese");
		credit.setParty( ((Supplier<Party>)() -> {
			Party p = new Party();
			p.setId(1L);
			p.setName("Grocery Outlet");
			return p;
		}).get() );
		
		return credit;
	}
	
}
