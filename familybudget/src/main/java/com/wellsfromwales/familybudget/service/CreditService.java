package com.wellsfromwales.familybudget.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.wellsfromwales.familybudget.model.Credit;

@Repository
public class CreditService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public Credit findCreditById(Long id) {
		return entityManager.find(Credit.class, id);
	}

}
